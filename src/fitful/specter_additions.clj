(ns fitful.specter-additions
    (:require [com.rpl.specter :refer :all]))


(defcollector collectf [f]
              (collect-val [this structure]
                           (do (println structure)
                               (f structure))))

(defmacro exe
          [& code]
          (let [structure 'structure]
               `(fn [~structure] ~@code true)))

(defmacro skip
          ([]
           '(subselect INDEXED-VALS (fn [[x _]] (> x 1)) LAST))
          ([n]
           `(subselect INDEXED-VALS (fn [[~@(list 'x '_)]] (>= ~(symbol 'x) ~n)) LAST)))
