(ns fitful.super-threader
  (:refer-clojure :exclude [->])
  (:require [com.rpl.specter :refer :all]))

(defn insert-code
  [code-to-insert insertion-target]
  (transform
    [(if-path #(and (sequential? %) (not (some '#{fn fn*} %)) (some '#{_} %)) [ALL #(= '_ %)] STAY)]
    (fn [insertion-point]
      (cond
        (= '_ insertion-point) code-to-insert
        (symbol? insertion-point) `(~insertion-point ~code-to-insert)
        (or (= 'fn (first insertion-point)) (= 'fn* (first insertion-point))) `(~insertion-point ~code-to-insert)
        :else `(~(first insertion-point) ~code-to-insert ~@(rest insertion-point))))
    insertion-target))

(defmacro ->
  [& code]
  (reduce insert-code code))

